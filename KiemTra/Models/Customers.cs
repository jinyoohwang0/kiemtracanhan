﻿using System.ComponentModel.DataAnnotations;
namespace KiemTra.Models
{
   
        public class Customers
        {
            public int Id { get; set; }
            [Required, StringLength(100)]
            public string FirstName { get; set; }
            [Range(0.01, 10000.00)]
            public string LastName { get; set; }
            public string ContactAndAddress { get; set; }
            public string UserName { get; set; }


            public Accounts? Accounts { get; set; }
            public Transactions? Transactions { get; set; }

            public string Password { get; set; }
            public string AddTextHere { get; set; }
        }
   
}
