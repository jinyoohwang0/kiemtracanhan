﻿using System.ComponentModel.DataAnnotations;
namespace KiemTra.Models
{

    public class Logs
    {
        public string Id { get; set; }
        public string TransactionalId { get; set; }

        public string LoginDate { get; set; }
        public string LoginTime { get; set; }
        public Reports? Reports { get; set; }


        public Transactions? Transactions { get; set; }
        public string AddTextHere { get; set; }
    }
}
