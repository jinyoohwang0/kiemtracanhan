﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
   
        public class Employees
        {
            public int Id { get; set; }
            [Required, StringLength(100)]
            public string FirstName { get; set; }
            [Range(0.01, 10000.00)]
            public string LastName { get; set; }
            public string ContactAndAddress { get; set; }
            public string UserNameAndPassword { get; set; }

            public Transactions? Transactions { get; set; }

            public string AddTextHere { get; set; }
        }
    
}
