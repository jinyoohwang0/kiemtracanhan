﻿using System.ComponentModel.DataAnnotations;
namespace KiemTra.Models
{
    public class Transactions
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }

        public Reports? Reports { get; set; }

        public string AddTextHere { get; set; }
    }
}
